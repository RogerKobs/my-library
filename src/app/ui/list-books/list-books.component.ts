import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Book } from 'src/app/model/book.model';

@Component({
    selector: 'app-list-books',
    templateUrl: './list-books.component.html',
    styleUrls: ['./list-books.component.css'],
})
export class ListBooksComponent implements OnInit {

    books: Book[] = []

    formSearch = this.formBuilder.group({
        searchType: [1, [Validators.required]],
        searchText: ['', [Validators.required]]
    })

    constructor(private formBuilder: FormBuilder) { }

    ngOnInit(): void {
    }

    clearSearch() {
        this.searchText.setValue("")
        this.searchType.setValue(1)
    }

    get searchType() {
        return this.formSearch.controls.searchType
    }

    get searchText() {
        return this.formSearch.controls.searchText
    }
}
