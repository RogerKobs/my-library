import { Component, OnInit } from '@angular/core';
import { Book } from 'src/app/model/book.model';

@Component({
    selector: 'app-details-book',
    templateUrl: './details-book.component.html',
    styleUrls: ['./details-book.component.css']
})
export class DetailsBookComponent implements OnInit {

    book: Book = new Book()

    constructor() { }

    ngOnInit(): void {

    }

}
