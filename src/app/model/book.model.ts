export class Book {
    public id: number
    public title: string
    public author: string
    public edition: number
    public publicationYear: number
    public summary: string
    public publisher: string
    public pages: number
    public isbn: string
}

export class RegisterBook {
    public title: string
    public author: string
    public edition: number
    public publicationYear: number
    public summary: string
    public publisher: string
    public pages: number
    public isbn: string

    constructor(title: string,
                author: string,
                edition: number,
                publicationYear: number,
                summary: string,
                publisher: string,
                pages: number,
                isbn: string) {
        
        this.title = title
        this.author = author
        this.edition = edition
        this.publicationYear = publicationYear
        this.summary = summary
        this.publisher = publisher
        this.pages = pages
        this.isbn = isbn
    }
}